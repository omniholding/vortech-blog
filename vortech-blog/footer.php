<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						 ) );
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>

			<div class="site-info">
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
<footer class="footer-navigation">
    <div class="container">
        <div class="social-newsletter">
            <ul class="social">
                <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="index.html#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="index.html#"><i class="fa fa-pinterest"></i></a></li>
            </ul>

            <form action="index.html#" class="newsletter">
                <p class="message">Join Our Newsletter</p>

                <fieldset>
                    <input id="newsletterEmail" name="newsletterEmail" type="text" required />
                    <label for="newsletterEmail">Email Address *</label>

                    <button type="submit"><i class="fa fa-plus-circle"></i></button>
                </fieldset>
            </form>
        </div>

        <div class="navigation">
            <a href="index.html#" class="logo"><img src="http://vortech.com/images/logo_white.png" alt="Vortech"></a>

            <ul class="nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="index.html#">Technology</a></li>
                <li><a href="index.html#">Blog</a></li>
                <li><a href="checkout.php.html">Buy Now</a></li>

                <li><a href="index.html#">Our Guarantee</a></li>
                <li><a href="index.html#">Contact Us</a></li>
            </ul>
        </div>
    </div>
</footer>

<footer class="main-footer">
    <div class="container">

        <div class="payment-type">
            <p>We Accept All Popular Payment Methods:</p>
            <i class="fa fa-cc-amex"></i>
            <i class="fa fa-cc-visa"></i>
            <i class="fa fa-cc-mastercard"></i>
            <i class="fa fa-cc-discover"></i>
        </div>

        <div class="navigation">

            <p class="copyright">© 2016 Vortech - All rights reserved.</p>

            <ul class="nav">
                <li><a href="index.html#">Terms & Conditions</a></li>
                <li><a href="index.html#">Privacy Policy</a></li>
            </ul>

        </div>

    </div>
</footer>
</body>
</html>
